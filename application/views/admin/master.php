<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Admin - <?php echo $title;?> </title>
    <!-- BOOTSTRAP STYLES-->
   
    <link href="<?php echo base_url();?>asset/admin/assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="<?php echo base_url();?>asset/admin/assets/css/font-awesome.css" rel="stylesheet" />
    <!-- MORRIS CHART STYLES-->
    <link href="<?php echo base_url();?>asset/admin/assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="<?php echo base_url();?>asset/admin/assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
     <link href="<?php echo base_url();?>asset/admin/assets/css/toastr.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>asset/admin/assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html"><?php echo $this->session->userdata('admin_name');?></a> 
            </div>
            <div style="color: white;
            padding: 15px 50px 5px 50px;
            float: right;
            font-size: 16px;"> Last access : 30 May 2014 &nbsp; <a href="<?php echo base_url();?>administrator/logout" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li class="text-center">
                        <img src="<?php echo base_url();?>asset/admin/assets/img/find_user.png" class="user-image img-responsive"/>
                    </li>


                    <li>
                        <a class="active-menu"  href="<?php echo base_url();?>administrator"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
                    </li>
                    <li>
                        <a  href="<?php echo base_url();?>administrator/addCategory"><i class="fa fa-desktop fa-3x"></i> Add Category</a>
                    </li>
                    <li>
                        <a  href="<?php echo base_url();?>administrator/manageCategory"><i class="fa fa-desktop fa-3x"></i>Manage Category</a>
                    </li>
                    <li>
                        <a  href="<?php echo base_url();?>administrator/addBrand"><i class="fa fa-desktop fa-3x"></i> Add Brand</a>
                    </li>
                    <li>
                        <a  href="<?php echo base_url();?>administrator/addProduct"><i class="fa fa-desktop fa-3x"></i> Add Product</a>
                    </li>

                </ul>

            </div>

        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <?php echo $admin_mainContent;?>
            </div>
            <!-- /. PAGE WRAPPER  -->
        </div>
        <!-- /. WRAPPER  -->
        <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
        <!-- JQUERY SCRIPTS -->
        <script src="<?php echo base_url();?>asset/admin/assets/js/jquery-1.10.2.js"></script>
        <!-- BOOTSTRAP SCRIPTS -->
        <script src="<?php echo base_url();?>asset/admin/assets/js/bootstrap.min.js"></script>
        <!-- METISMENU SCRIPTS -->
        <script src="<?php echo base_url();?>asset/admin/assets/js/jquery.metisMenu.js"></script>
        <!-- MORRIS CHART SCRIPTS -->
        <script src="<?php echo base_url();?>asset/admin/assets/js/morris/raphael-2.1.0.min.js"></script>
        <script src="<?php echo base_url();?>asset/admin/assets/js/morris/morris.js"></script>
        <!-- CUSTOM SCRIPTS -->

        <script src="<?php echo base_url();?>asset/admin/assets/js/dataTables/jquery.dataTables.js"></script>
        <script src="<?php echo base_url();?>asset/admin/assets/js/dataTables/dataTables.bootstrap.js"></script>
            <script>
                $(document).ready(function () {
                    $('#dataTables-example').dataTable();
                });
        </script>
         <!-- CUSTOM SCRIPTS -->
        <script src="<?php echo base_url();?>asset/admin/assets/js/custom.js"></script>
        <script src="<?php echo base_url();?>asset/admin/assets/js/toastr.min.js"></script>


        <script type="text/javascript">
            <?php if ($this->session->flashdata('message')) {?>
                toastr.success("<?php echo $this->session->flashdata('message'); ?>");
            <?php }elseif ($this->session->flashdata('error')) {?>
                toastr.warning("<?php echo $this->session->flashdata('error'); ?>");
            <?php }elseif($this->session->flashdata('danger')){?>
                 toastr.error("<?php echo $this->session->flashdata('danger'); ?>");
            <?php } ?>
        </script>


        </body>
</html>
