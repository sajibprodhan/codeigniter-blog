
<div class="col-md-12">
	<!-- Form Elements -->
	<div class="panel panel-default">
		<div class="panel-heading">
			Add Brand Form
		</div>

		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<form role="form" action="<?php echo base_url();?>administrator/saveBrand" method="POST">
						<div class="form-group">
							<label>Brand Name : </label>
							<input name="brand_name" class="form-control" placeholder="PLease Enter Keyword" required="">
						</div>

						<div class="form-group">
							<label>Brand Description : </label>
							<textarea name="brand_desc" name="" id="" class="form-control"  cols="30" rows="10"></textarea>
						</div>

				
						<div class="form-group">
							<label>Publication status</label>
							<select class="form-control" name="level_access">
								<option value="1">Publish</option>
								<option value="0">Unpublish</option>
							</select>
						</div>
						
						<button type="submit" class="btn btn-primary">Submit Button</button>
						<button type="reset" class="btn btn-default">Reset Button</button>

					</form>



				</div>
			</div>
		</div>
	</div>
	<!-- End Form Elements -->
</div>