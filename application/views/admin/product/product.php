
<div class="col-md-12">
	<!-- Form Elements -->
	<div class="panel panel-default">
		<div class="panel-heading">
			Form Element Examples
		</div>

		<div class="panel-body">
			<div class="row">
				<div style="color: green">	
					<?php 
						$message = $this->session->userdata('message');
						if(isset($message)){
							echo $message;
							$this->session->unset_userdata('message');
						}
					?>
				</div>

				<div class="col-md-6">
					<form role="form" action="<?php echo base_url();?>administrator/saveProduct" method="POST" eenctype="multipart/form-data">
						<div class="form-group">
							<label>Product Name : </label>
							<input name="pro_name" class="form-control" placeholder="PLease Enter Keyword">
						</div>
						<div class="form-group">
							<label>Category Name :</label>
							<select class="form-control" name="cat_name">
								<option>One Vale</option>
								
							</select>
						</div>

						<div class="form-group">
							<label>Brand Name :</label>
							<select class="form-control" name="brand_name">
								<option>One Vale</option>
							</select>
						</div>
						<div class="form-group">
							<label>Product Description : </label>
							<textarea name="pro_desc" class="form-control" cols="30" rows="10"></textarea>
						</div>	

						<div class="form-group">
							<label>Product Description : </label>
							<textarea name="short_desc" class="form-control" cols="30" rows="6"></textarea>
						</div>						
						<div class="form-group">
							<label>File input</label>
							<input type="file">
						</div>


						<div class="form-group">
							<label>Publications Status</label>
							<select class="form-control" name="level_access">
								<option value="1">Publish</option>
								<option value="0">Unpublish</option>
							</select>
						</div>

						<button type="submit" class="btn btn-primary">Submit Button</button>
						<button type="reset" class=" btn btn-default">Reset Button</button>

					</form>



				</div>
			</div>
		</div>
	</div>
	<!-- End Form Elements -->
</div>