<div class="row">
	<div class="col-md-12">
		<!-- Advanced Tables -->
		<div class="panel panel-default">
			<div class="panel-heading">
				Advanced Tables
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
							<tr>
								<th>No</th>
								<th>Cat Name</th>
								<th>Cat Description</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							
							<?php  
							$i = 0;
							foreach ($allCategory as $category):?>
									
									<tr class="odd gradeX">
										<td><?php echo $i;?></td>
										<td><?php echo $category->cat_name;?></td>
										<td>
											<?php
												echo substr($category->cat_desc, 0, 50);
												echo strlen($category->cat_desc) > 50 ? " ...." : "";
											?>
										</td>
										<td><?php echo ($category->level_access ==1 ) ? 'Publish': 'Unpublish' ;?> </td>
										<td>
											<a href="<?php echo base_url();?>administrator/editCategory/<?php echo $category->id;?>" class="btn btn-danger btn-xs">Edit</a>

											<a href="<?php echo base_url();?>administrator/deleteCat/<?php echo $category->id;?>" class="btn btn-danger btn-xs" onclick="return confirm('Are you want sure delete it !!')">Delete</a>
										</td>
									</tr>
							<?php $i++; endforeach; ?>
						
						</tbody>
					</table>
				</div>

			</div>
		</div>
		<!--End Advanced Tables -->
	</div>
</div>
<!-- /. ROW  -->