
<div class="col-md-12">
	<!-- Form Elements -->
	<div class="panel panel-default">
		<div class="panel-heading">
			Add Category Form
		</div>

		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					
					<form role="form" action="<?php echo base_url();?>administrator/uddateCategory" method="POST">
						<div class="form-group">
							<label>Category Name : </label>
							<input name="cat_name" type="text" class="form-control" placeholder="PLease Enter Keyword" value="<?php echo $category_inform->cat_name;?>" required="">

							<input name="id" type="hidden" class="form-control" placeholder="PLease Enter Keyword" value="<?php echo $category_inform->id;?>" required="">
						</div>

						<div class="form-group">
							<label>Category Description</label>
							<textarea name="cat_desc" id="" class="form-control"  cols="30" rows="10"><?php echo $category_inform->cat_desc;?></textarea>
						</div>

				
						<div class="form-group">

							<label>Publication status</label>
							<select class="form-control" name="level_access">
																									
								<option value="1" 
									<?php  
										if($category_inform->level_access == '1' ){
										echo "selected";
									} ?>>Publish
								</option>


								<option value="0"
								<?php 
									if($category_inform->level_access == '0' ){
										echo "selected";
									} ?>>Unpublish
								</option>					
							</select>
						</div>
						
						<button type="submit" class="btn btn-primary">Update Category</button>
						<button type="reset" class="btn btn-default">Reset Button</button>

					</form>



				</div>
			</div>
		</div>
	</div>
	<!-- End Form Elements -->
</div>