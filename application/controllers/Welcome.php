<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function index()
	{
		$data = [];
		$data['title'] = 'Welcome';
		$data['slider'] = 1;
		$data['main_content'] = $this->load->view('frontpage','', true);
		$this->load->view('layouts/master', $data);
	}

	public function support()
	{
		$data = [];
		$data['title'] = 'Support';
		$data['main_content'] = $this->load->view('pages/support','', true);
		$this->load->view('layouts/master', $data);
	}

	public function about()
	{
		$data = [];
		$data['title'] = 'About-us';
		$data['main_content'] = $this->load->view('pages/about','', true);
		$this->load->view('layouts/master', $data);
	}

	public function blog()
	{
		$data = [];
		$data['title'] = 'Blog-page';
		$data['main_content'] = $this->load->view('pages/blog','', true);
		$this->load->view('layouts/master', $data);
	}

	public function contact()
	{
		$data = [];
		$data['title'] = 'Contact';
		$data['main_content'] = $this->load->view('pages/contact','', true);
		$this->load->view('layouts/master', $data);
	}



}
