<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {
	function __construct() {
        parent::__construct();
        
        $admin_id = $this->session->userdata('id');

        if($admin_id == NULL){
        	redirect('Admin');
        }
        
    }

	public function index()
	{
		$data = array();
		$data['title'] = 'dashboard';
		$data['admin_mainContent'] = $this->load->view('admin/pages/dashboard','', true);
		
		$this->load->view('admin/master', $data);
	
	}

	public function logout()
	{
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('admin_name');
		$data = array();
		
		$this->session->set_flashdata('message', 'You are successfully logout !!');
		redirect('Admin');
	}

	public function addCategory()
	{
		$data = array();
		$data['title'] = 'Add Category';
		$data['admin_mainContent'] = $this->load->view('admin/category/addCategory','', true);
		$this->load->view('admin/master', $data);
	}


	public function addBrand()
	{
		$data = array();
		$data['title'] = 'Add Brand';
		$data['admin_mainContent'] = $this->load->view('admin/brand/addBrand','', true);
		$this->load->view('admin/master', $data);
	}


	public function addProduct()
	{
		$data = array();
		$data['title'] = 'Add Product';
		$data['admin_mainContent'] = $this->load->view('admin/product/product','', true);
		$this->load->view('admin/master', $data);
	}


	public function saveCategory()
	{
		$data = array();
		$data['cat_name'] = $slug = $this->input->post('cat_name');
		$data['cat_desc'] = $this->input->post('cat_desc');

		$data['cat_slug'] = url_title($slug);
		$data['level_access'] = $this->input->post('level_access');

		$datImg = array();
		$postId = $this->Administrator_model->saveCategory_info($data, $datImg);
		

		if ($_SERVER["REQUEST_METHOD"] == "POST") {

		    $target_dir = "./uploads/";
		    $allow_types = array('jpg','png','jpeg','gif');
		    
		    $images_arr = array();
		    foreach($_FILES['images']['name'] as $key => $val){

		        $image_name = $_FILES['images']['name'][$key];
		        $tmp_name   = $_FILES['images']['tmp_name'][$key];
		        $size       = $_FILES['images']['size'][$key];
		        $type       = $_FILES['images']['type'][$key];
		        $error      = $_FILES['images']['error'][$key];
		        
		        
		        $file_name = basename($_FILES['images']['name'][$key]);
		        $targetFilePath = $target_dir . $file_name;

	
		        $file_type = pathinfo($targetFilePath,PATHINFO_EXTENSION);
		        if(in_array($file_type, $allow_types)){    
		            
		            if(move_uploaded_file($_FILES['images']['tmp_name'][$key],$targetFilePath)){
		            	$datImg['image_url'] = $targetFilePath;
		                //$images_arr[] = $targetFilePath;
		            }
		        }
		    }
		    
		   
		   
			}





			$datImg['cat_id '] = $postId;















			$this->session->set_flashdata('message', 'Category has been saved !!');
			redirect('Administrator/addCategory');

		}

		public function saveBrand()
		{
			$data = array();
			$data['brand_name'] = $this->input->post('brand_name');
			$data['brand_desc'] = $this->input->post('brand_desc');
			$data['level_access'] = $this->input->post('level_access');
			$this->Administrator_model->saveBrand_info($data);
			$this->session->set_flashdata('message', 'Brand has been saved !!');
			redirect('Administrator/addBrand');

		}
		public function manageCategory()
		{
			$data = array();
			$data['title'] = 'Manage Category';
			$data['allCategory'] = $this->Administrator_model->get_all_category();
			$data['admin_mainContent'] = $this->load->view('admin/category/manageCategory',$data, true);
			$this->load->view('admin/master', $data);

		}

		public function editCategory($id='')
		{
			$data = array();
			$data['title'] = 'Edit Category';
			$data['category_inform'] = $this->Administrator_model->select_cat_by_id($id);
			$data['admin_mainContent'] = $this->load->view('admin/category/editCategory',$data, true);
			$this->load->view('admin/master', $data);
		}

		public function uddateCategory()
		{
			$data = array();
			$data['cat_name'] = $slug = $this->input->post('cat_name');
			$data['cat_desc'] = $this->input->post('cat_desc');
			$data['cat_slug'] = url_title($slug);
			$data['level_access'] = $this->input->post('level_access');
			$categoryId = $this->input->post('id');
			$this->Administrator_model->updateCateogory_by_id($data, $categoryId);
			$this->session->set_flashdata('message', 'Category has been Updated !!');
			redirect('Administrator/manageCategory');
		}

		public function deleteCat($id)
		{
			$this->Administrator_model->delete_cat_by_id($id);
			$this->session->set_flashdata('danger', 'Category has been deleted !!');
			redirect('Administrator/manageCategory');
		}








}
