<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct() {
        parent::__construct();
        
        $admin_id = $this->session->userdata('id');

        if($admin_id != NULL){
        	redirect('Administrator');
        }
        
    }


	public function index()
	{
		$this->load->view('admin/login/login');
	}

	public function admin_login_check()
	{
		
		$admin_email = $this->input->post('admin_email', true);
		$admin_pass = $this->input->post('admin_pass', true);
		$result = $this->Admin_auth_model->admin_login_check_info($admin_email, $admin_pass);
		
		if(! $result){
			$this->session->set_flashdata('error', 'You have entered wrong user name or password !!');
			redirect('Admin');
		}else{
			$data['id'] = $result->id;
			$data['admin_name'] = $result->admin_name;
			$this->session->set_userdata($data);
			redirect('Administrator');
		}
		
	}
}
